import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/api/role/list',
    method: 'post',
    params: data
  })
}

export function createRow(data) {
  return request({
    url: '/api/role/add',
    method: 'post',
    data
  })
}

export function updateRow(data) {
  return request({
    url: '/api/role/update',
    method: 'post',
    data
  })
}
export function deleteRow(data) {
  return request({
    url: '/api/role/delete',
    method: 'post',
    params: data
  })
}

export function getRow(data) {
  return request({
    url: '/api/role/detail',
    method: 'post',
    params: data
  })
}

export function getRoleUserByRole(data) {
  return request({
    url: '/api/role/getRoleUserByRole',
    method: 'post',
    params: data
  })
}
export function getRolePermissionByRole(data) {
  return request({
    url: '/api/role/getRolePermissionByRole',
    method: 'post',
    params: data
  })
}
export function getRoleWxAccountByRole(data) {
  return request({
    url: '/api/role/getRoleWxAccountByRole',
    method: 'post',
    params: data
  })
}

export function saveRoleUser(data) {
  return request({
    url: '/api/role/saveRoleUser',
    method: 'post',
    params: data
  })
}

export function saveRoleMenu(data) {
  return request({
    url: '/api/role/saveRoleMenu',
    method: 'post',
    params: data
  })
}

export function saveRoleWxAccount(data) {
  return request({
    url: '/api/role/saveRoleWxAccount',
    method: 'post',
    params: data
  })
}
