import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/api/wx/menu/list',
    method: 'post',
    params:data
  })
}

export function createRow(data) {
  return request({
    url: '/api/wx/menu/add',
    method: 'post',
    data
  })
}

export function updateRow(data) {
  return request({
    url: '/api/wx/menu/update',
    method: 'post',
    data
  })
}
export function deleteRow(data) {
  return request({
    url: '/api/wx/menu/delete',
    method: 'post',
    params:data
  })
}

export function listTreeMenu() {
  return request({
    url: '/api/wx/menu/listTreeMenu',
    method: 'post'
  })
}
export function validateData(data) {
  return request({
    url: '/api/wx/menu/validateData',
    method: 'post',
    data
  })
}
export function syncAccountMenu(data) {
  return request({
    url: '/api/wx/menu/syncAccountMenu',
    method: 'post',
    data
  })
}
